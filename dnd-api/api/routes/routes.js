'use strict';
module.exports = function (app) {
    var characterList = require('../controllers/controller');

    // todoList Routes
    app.route('/characters')
        .get(characterList.list_all_characters)
        .post(characterList.create_a_character);


    app.route('/characters/character:Id')
        .get(characterList.read_a_character)
        .put(characterList.update_a_character)
        .delete(characterList.delete_a_character);
};