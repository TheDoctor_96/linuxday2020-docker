'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var CharacterSchema = new Schema({
    name: {
        type: String,
        required: 'Inserisci il nome'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
    stats: {
        FOR: { type: Number, default: 10 },
        DEX: { type: Number, default: 10 },
        COS: { type: Number, default: 10 },
        INT: { type: Number, default: 10 },
        SAG: { type: Number, default: 10 },
        CAR: { type: Number, default: 10 }
    }
});

module.exports = mongoose.model('Character', CharacterSchema);