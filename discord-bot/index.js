require('dotenv').config();
const Discord = require('discord.js');
const axios = require('axios');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;

const stats = {
  magnus: {
    FOR: 16,
    DEX: 15,
    COS: 14,
    INT: 10,
    SAG: 16,
    CAR: 8
  }
}

const dice = [
  {
    name: ':d20:',
    value: 20
  },
  {
    name: ':d12:',
    value: 12
  },
  {
    name: ':d10:',
    value: 10
  },
  {
    name: ':d8:',
    value: 8
  },
  {
    name: ':d6:',
    value: 6
  },
  {
    name: ':d4:',
    value: 4
  },
  {
    name: ':d2:',
    value: 2
  },
]

function random (low, high) {
  return Math.trunc(Math.random() * ((high + 1) - low) + low)
}

function countOccurences (string, word) {
  return string.split(word).length - 1;
}

bot.login(TOKEN);

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on('message', msg => {
  if (msg.content === '!characters') {
    axios.get('http://api:3000/characters').then(res => {
      msg.reply(JSON.stringify(res.data))
    })
  }

  if (msg.content.includes('<:d')) {
    let rolls = [];
    let result = 0
    let responseString = '\n'
    dice.forEach(d => {

      let occurrenices = countOccurences(msg.content, d.name)
      for (let index = 0; index < occurrenices; index++) {
        rolls.push({ name: `d${d.value}`, value: random(1, d.value) })
      }

    })
    rolls.forEach(roll => {
      responseString = responseString.concat(`${roll.name}: ${roll.value}\n`);
      result += roll.value
    })
    responseString = responseString.concat(`TOTALE: ${result}`)
    msg.reply(responseString);
  }
});
